var gulp = require('gulp');
var sass = require("gulp-sass");
var concat = require("gulp-concat");
var watch = require("gulp-watch");
var clean = require("gulp-clean-css");



gulp.task("sass",function(){
    return watch("www/css/*.scss",function () {
        gulp.src(["node_modules/bootstrap/scss/bootstrap.scss","www/css/*.scss"])
            .pipe(sass())
            .pipe(concat("bundle.css"))
            .pipe(clean())
            .pipe(gulp.dest("www/css/"));
    });
});


gulp.task("default",["sass"]);
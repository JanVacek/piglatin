<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI\Form;


class HomepagePresenter extends Nette\Application\UI\Presenter
{

    public $translateControlFactory;

    public function __construct(\ITranslateFormFactory $translateFormFactory)
    {
        parent::__construct();
        $this->translateControlFactory = $translateFormFactory;
    }

    protected function createComponentTranslateForm()
    {
        return $this->translateControlFactory->create();
    }



}

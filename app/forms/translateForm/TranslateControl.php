<?php

use Nette\Application\UI\Form;

class TranslateControl extends \Nette\Application\UI\Control
{

    const VOWELS = "aeiouy";
    const CONSONANTS = "bcdfghjklmnpqrstvwxz";

    public function render():void
    {
        $this->getTemplate()->setFile(__DIR__ . '/TranslateControl.latte'); //nastavení vlastní šablonu komponenty
        $this->getTemplate()->render(); //vykreslení pod {control translate}
    }


    /**
     * @return Form
     */
    protected function createComponentForm():Form
    {
        $form = new Nette\Application\UI\Form();
        $form->addProtection('Session expired, please refresh current page');
        $form->addTextArea("text","English text")->setRequired("Please write some english text");
        $form->addSubmit('submit',"Translate");
        $form->onSuccess[] = function (Nette\Application\UI\Form $form, array $values) {
            $this->processForm($form, $values);
        };
        return $form;
    }

    /**
     * @param Form $form
     * @param array $values
     * @throws Exception
     */
    private function processForm(Form $form, array $values):void
    {
        $values = $form->values;
        //odstraní přebytečné mezery
        $input = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $values->text)));
        $arr_words = explode(" ",$input); //rozdělí řetězec do pole
        $result = "";
        foreach($arr_words as $word){
            if(strlen($word)>0)
            $result .= $this->translateWord($word); // poskládá result z přeložených slov
        }
        $this->template->result = $result;  // odešle do template TranslateControl.latte
    }


    /**
     * @param string $word
     * @return string
     */
    private function translateWord(string $word) :string
    {
        $word = strtolower($word); //sjednocení na malá písmena

        for($i = 0; $i < strlen(self::VOWELS);$i++){// prochází každý vowel
            if($word[0] == self::VOWELS[$i]) // pokud se jedná o vowel, ukončí metodu a vrátí upravené slovo
                return $word."way ";
        }
        $res = "";
        for($i = 0; $i < strlen($word);$i++){ //prochází každé písmeno slova
            $res_prev = $res; // uloží minulý stav
            for($cons = 0; $cons < strlen(self::CONSONANTS);$cons++){ // prochází každý consonant
                if($word[$i] == self::CONSONANTS[$cons])// pokud se jedná o consonant, zřetězí ho za poslední
                    $res .= $word[$i];
            }
            if($res_prev == $res)break; // porovná, jestli se něco změnilo(zda už řetězec consonantů byl přerušen a cyklus ukončí
        }
        $word = substr($word,strlen($res))."-".$res."ay ";
        return $word;
    }
}
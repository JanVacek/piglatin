<?php

interface ITranslateFormFactory
{

    /**
     * @return TranslateControl
     */
    public function create();

}